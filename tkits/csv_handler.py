#! /usr/bin/env python
# -*- coding: utf-8 -*-


class NotColumnName(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return '"%s" not a csv column name' % self.name


class CSVHandler(object):
    def __init__(self, file_name, seq=',', float_index=False):
        data = []
        # 行数
        nrow = 0
        fp = open(file_name)
        for line in fp:
            elements = line.strip().split(seq)
            # 列数
            ncol = len(elements)
            if nrow == 0:
                # 列名
                col_names = elements
            else:
                if float_index:
                    data.extend([float(i) for i in elements])
                else:
                    data.extend(elements)
            nrow += 1
        fp.close()
        self.data = data
        self.nrow = nrow - 1
        self.ncol = ncol
        self.col_names = col_names
        self._i = 0
        # 默认迭代行
        self._current_iter = 'row'

    def get_row(self, i):
        '''
        获取一行数据，i是行数
        '''
        row_data = None
        if 0 <= i < self.nrow:
            row_data = [self.data[self.ncol * i + col] for col in range(self.ncol)]
        return row_data

    def get_col(self, i):
        '''
        获取一列数据，i是列数或列名
        '''
        col_data = None
        if isinstance(i, (str, unicode)):
            try:
                i = self.col_names.index(i)
            except ValueError:
                raise NotColumnName(i)

        if 0 <= i < self.ncol:
            col_data = [self.data[self.ncol * row + i] for row in range(self.nrow)]
        return col_data

    def switch(self):
        '''
        行迭代，列迭代间的切换
        '''
        self._current_iter = 'col' if self._current_iter == 'row' else 'row'
        return self

    def __iter__(self):
        return self

    def next(self):
        if self._current_iter == 'row':
            if self._i < self.nrow:
                data_rows = self.get_row(self._i)
                self._i += 1
                return data_rows
            else:
                self._i = 0
                raise StopIteration
        else:
            if self._i < self.ncol:
                data_cols = self.get_col(self._i)
                self._i += 1
                return data_cols
            else:
                self._i = 0
                raise StopIteration

    def __getitem__(self, i):
        if isinstance(i, (str, unicode)) or self._current_iter == 'col':
            value = self.get_col(i)
        else:
            value = self.get_row(i)
        return value


if __name__ == '__main__':
    pass
