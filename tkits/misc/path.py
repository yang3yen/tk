#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import re


__all__ = ['mkdirs', 'splitext', 'joinext', 'list_dir']


def mkdirs(dst, mode=0777):
    try:
        os.makedirs(dst, mode=mode)
    except OSError:
        pass


def splitext(path):
    """
    >>> splitext('a.txt')
    ('a', 'txt')
    """
    root, ext = os.path.splitext(path)
    return root, ext[1:]


def joinext(name, ext, sep='.'):
    """
    >>> joinext('a', 'txt')
    'a.txt'
    """
    return sep.join((name, ext))


def list_dir(root, ftype='f', pattern=None, path=True, recursive=True):
    """
    @root: root dir
    @ftype: file type,
        'f' --> general file,
        'd' --> dir,
        'a' --> file and dir
    @pattern: search pattern
    @path: if True, return path else name
    @recursive: if recursive is True, list files or subdirs recursively
    Return a generator of subdirs or files
    """
    for dirpath, subdirs, filenames in os.walk(root):
        subs = []
        if ftype == 'f':
            subs = filenames
        elif ftype == 'd':
            subs = subdirs
        elif ftype == 'a':
            subs.extend(subdirs)
            subs.extend(filenames)
        else:
            break
        # dirname or filename filter
        for sub in subs:
            _sub = os.path.join(dirpath, sub)
            if pattern is not None:
                if re.search(pattern, _sub) is None:
                    continue
            if path:
                # abspath
                yield os.path.abspath(_sub)
            else:
                # name
                yield sub
        # recursive
        if not recursive:
            break


if __name__ == '__main__':
    pass
