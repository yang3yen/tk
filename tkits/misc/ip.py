#!/usr/bin/env python
# -*- coding: utf-8 -*-


import socket
from subprocess import Popen, STDOUT

try:
    # py3k
    from subprocess import DEVNULL
except ImportError:
    # py2k
    from os import devnull
    DEVNULL = open(devnull, 'wb')


__all__ = ['local_ip', 'ping']


def local_ip():
    """
    Get local ip address.

    :return: ip address
    :rtype str
    """
    _address = ('8.8.8.8', 53)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(_address)
        ip = s.getsockname()[0]
    except socket.error:
        ip_list = [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2]]
        # 有多个ip时, 筛掉'127.'的ip
        if len(ip_list) > 1:
            ip_list = [ip for ip in ip_list if not ip.startswith('127.')]
        ip_list = ip_list[:1]
        if ip_list:
            ip = ip_list[0]
        else:
            ip = ''
    finally:
        s.close()
    return ip


def ping(ip):
    """
    Test whether a ip address can be ping succeed.

    :param ip: Ip address.
    :type: basestring
    :return: True or False
    :rtype bool
    """
    _args = ['ping', '-c', '1', '%s' % ip]
    p = Popen(args=_args, stdout=DEVNULL, stderr=STDOUT)
    p.communicate()
    return p.returncode == 0
