#!/usr/bin/env python
# -*- coding: utf-8 -*-


from functools import partial
from datatype import isseq
from multiprocessing import Pool as _Pool, cpu_count


__all__ = ['split_job']


def multi_args_func(func, seq):
    return func(*seq)


def split_job(func, job_list, n_processes=None, wait_result=True,
              callback=None, **func_kwargs):
    cpu_number = cpu_count()

    n_jobs = len(job_list)
    if not n_jobs:
        raise ValueError('argument job_list at least have one item.')

    if n_processes <= 0 or n_processes is None:
        n_processes = min(cpu_number, n_jobs)
    else:
        n_processes = min(n_processes, n_jobs)

    func = partial(func, **func_kwargs)
    # 进程池
    pool = _Pool(processes=n_processes)

    # 多个位置参数
    if all([isseq(args) for args in job_list]):
        func = partial(multi_args_func, func)

    if wait_result:
        job_results = pool.map_async(func, job_list, callback=callback).get(1 << 32)
    else:
        job_results = pool.map_async(func, job_list, callback=callback)

    # 关闭pool
    pool.close()
    pool.join()
    return job_results


if __name__ == '__main__':
    import time

    def f(x):
        time.sleep(2)
        return x ** 2

    start = time.time()
    split_job(f, range(10))
    print time.time() - start
