#!/usr/bin/env python
# -*- coding: utf-8 -*-


__all__ = ['loop_left_shift', 'loop_right_shift']


# 位运算
LEFT = 1
RIGHT = 0


def _loop_shift(num, offset, bit=8, order=LEFT):
    bin_str = '{:0>{width}}'.format(bin(num)[2:], width=bit)
    rem = offset % bit
    if order == RIGHT:
        rem = -rem
    return int(bin_str[rem:] + bin_str[:rem], 2)


def loop_left_shift(num, offset, bit=8):
    """
    循环向左移位(<<< i)
    :param num: 操作数
    :param offset: 左移位数
    :param bit: 比特数, 默认为8
    """
    return _loop_shift(num=num, offset=offset,
                       bit=bit)


def loop_right_shift(num, offset, bit=8):
    """
    循环向右移位(>>> i)
    :param num: 操作数
    :param offset: 右移位数
    :param bit: 比特数, 默认为8
    :return:
    """
    return _loop_shift(num=num, offset=offset,
                       bit=bit, order=RIGHT)
