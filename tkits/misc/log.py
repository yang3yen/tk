#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os.path as osp
import logging.config

from os import environ
from tkits.json_ext import load


home_dir = osp.expanduser('~')


def setup_logging(conf=None,
                  env_key='LOG_CFG'):
    """
    Setup logging configuration.
    """
    if not isinstance(conf, dict):
        if not conf:
            env_conf_rc = environ.get(env_key, None)
            default_conf_rc = osp.join(home_dir, '.logging')
            conf_rc = default_conf_rc if not env_conf_rc else env_conf_rc
        elif isinstance(conf, (str, unicode)):
            conf_rc = conf
        else:
            conf_rc = ''

        if not osp.exists(conf_rc):
            raise ValueError('logging module config file does not exist: %s' % conf_rc)
        conf = load(conf_rc)
    logging.config.dictConfig(conf)
