#!/usr/bin/env python
# -*- coding: utf-8 -*-


import string
import re
from random import choice


__all__ = ['import_str', 'ImportStr', 'random_str', 'gen_uuid8',
           'gen_uuid12', 'gen_uuid16', 'gen_uuid32', 'gen_uuid64', 'str2byte']


digit_letters = ''.join([string.digits, string.letters])


def gen_uuid8(length=8):
    return random_str(length=length)


def gen_uuid12(length=12):
    return random_str(length=length)


def gen_uuid16(length=16):
    return random_str(length=length)


def gen_uuid32(length=32):
    return random_str(length=length)


def gen_uuid64(length=64):
    return random_str(length=length)


def random_str(length=16, chars=digit_letters):
    return ''.join([choice(chars) for _ in xrange(length)])


class ImportStr(object):
    """
    ImportStr converts str to actual object, and return the object.
    >>> import_str = ImportStr()
    >>> import_str('re.match')
    <function re.match>
    """
    def __init__(self):
        self.cache = {}

    def __call__(self, s):
        import importlib
        if s in self.cache:
            return self.cache.get(s)
        tokens = s.split('.')
        try:
            obj = importlib.import_module(tokens[0])
            for i in xrange(1, len(tokens)):
                try:
                    obj = getattr(obj, tokens[i])
                except AttributeError:
                    obj = importlib.import_module(''.join(tokens[:i+1]))
            self.cache[s] = obj
            return obj
        except (ImportError, Exception):
            return None

import_str = ImportStr()


def str2byte(bstr):
    """
    解析string为Byte
    >>> str2byte('2k')
    2048
    """
    _ibyte = {
        'k': 10,    # 2**10B
        'm': 20,    # 2**20B
        'g': 30,    # 2**30B
        'p': 50,    # 2**50B
        'e': 60,    # 2**60B
        'z': 70,    # 2**70B
        'y': 80,    # 2**80B
        'b': 90,    # 2**90B
        'n': 100,   # 2**100B
        'd': 110,   # 2**110B
    }
    if not isinstance(bstr, (int, long, float, str, unicode)):
        return
    if isinstance(bstr, (int, long, float)):
        return bstr
    match = re.match(r'^(?P<value>[-+]?[0-9]*\.?[0-9]*)(?P<symbol>[kmgtpezybnd]?)$', bstr, re.I)
    if not match:
        res = None
    else:
        value = match.group('value')
        value = float(value)
        symbol = match.group('symbol')
        res = value * (2 ** _ibyte.get(symbol.lower(), 0))
    return res


if __name__ == '__main__':
    print str2byte('1M')
