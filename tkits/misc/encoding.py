#!/usr/bin/env python
# -*- coding: utf-8 -*-


def to_unicode(string,
               encodings=('gbk', 'utf8')):
    """
    Decode string to unicode using encodings
    :param string: a string
    :param encodings: encoding seq, ('gbk', 'utf8)
    :return: unicode string
    :rtype unicode
    """
    u_str = None
    for encoding in encodings:
        try:
            u_str = unicode(string, encoding)
        except UnicodeError:
            continue
        break
    if u_str is None:
        error_msg = '%s can not decode use %s' % (string, ' ,'.join(encodings))
        raise UnicodeError(error_msg)
    return u_str


if __name__ == '__main__':
    u = u'中文'
    assert to_unicode('中文') == u
    b = u.encode('gbk')
    assert to_unicode(b) == u
