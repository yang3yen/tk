#!/usr/bin/env python
# -*- coding: utf-8 -*-


import re
import boto3
import os.path

from tomorrow import threads
from botocore.client import ClientError
from .path import list_dir, mkdirs

__all__ = ['is_bucket', 'is_key', 'download', 'make_bucket', 'delete_bucket', 'upload']
s3 = boto3.resource('s3')
client = boto3.client('s3')
# 缓存bucket对象
bucket_cache = {}


def make_bucket(name, location='cn-north-1', **kwargs):
    """
    create a bucket in s3, default location is cn-north-1.
    """
    bucket = s3.Bucket(name=name)
    bucket_conf = {}
    bucket_conf.update({
        'CreateBucketConfiguration': {
            'LocationConstraint': location
        },
    })
    bucket_conf.update(kwargs)
    try:
        bucket.create(**bucket_conf)
    except ClientError:
        pass
    return bucket


def _delete_bucket_obj(obj):
    obj.delete()


def delete_bucket(bucket_uri, n=1, **kwargs):
    """
    delete a bucket or a file object in bucket.
    """
    bucket_name, remain = _parse_bucket_uri(bucket_uri)
    if not is_bucket(bucket_name):
        raise BucketNotExist(bucket_name)

    location = kwargs.get('location', 'cn-north-1')
    bucket = bucket_cache.setdefault(bucket_name,
                                     make_bucket(bucket_name, location))

    # not '' string
    if not remain:
        bucket_objs = bucket.objects.all()
    else:
        bucket_objs = bucket.objects.filter(Prefix=remain)

    # multiprocess
    if n > 1:
        delete_bucket_obj = threads(n=n)(_delete_bucket_obj)
    else:
        delete_bucket_obj = _delete_bucket_obj

    # filter
    pattern = kwargs.get('pattern')
    for obj in bucket_objs:
        # scan obj.key
        if pattern and not re.search(pattern, obj.key):
            continue
        # delete s3 obj
        delete_bucket_obj(obj)

    # delete bucket
    try:
        bucket.delete()
    except ClientError:
        pass


def is_bucket(name):
    """
    name: str类型
    判断bucket在s3中是否存在
    """
    exist = True
    try:
        client.head_bucket(Bucket=name)
    except ClientError:
        exist = False
    return exist


def is_key(bucket_name, key, **kwargs):
    """
    bucket_name, key: str or unicode类型
    判断key在bucket中是否存在
    """
    exist = True
    if not key:
        return False
    params = {
        'Bucket': bucket_name,
        'Key': key,
    }
    params.update(kwargs)
    try:
        client.head_object(**params)
    except ClientError:
        exist = False
    return exist


def _bucket_upload_file(bucket, file_path, file_key):
    bucket.upload_file(file_path, file_key)


def _upload_dir(dirpath, bucket_uri, n=1, **kwargs):
    """
    @bucket_uri: bucket_name/.../.../
    copy a dir contents to a s3 bucket.
    """
    bucket_name, remain = _parse_bucket_uri(bucket_uri)
    dirpath = os.path.abspath(dirpath)
    if not os.path.isdir(dirpath):
        raise OSError('%s is not a directory' % dirpath)

    if not is_bucket(bucket_name):
        raise BucketNotExist(bucket_name)

    # bucket缓存在bucket_cache中
    location = kwargs.get('location', 'cn-north-1')
    bucket = bucket_cache.setdefault(bucket_name, make_bucket(bucket_name, location=location))

    # mutli thread
    if n > 1:
        bucket_upload_file = threads(n=n)(_bucket_upload_file)
    else:
        bucket_upload_file = _bucket_upload_file

    default_kw = {
        'ftype': 'f',
        'path': True,
        'recursive': True,
        'pattern': None
    }

    # 将目录拷贝到桶中
    default_kw.update(kwargs)
    pattern = default_kw.get('pattern')
    base_dir = os.path.basename(dirpath) if pattern is None else ''

    for file_path in list_dir(dirpath, **default_kw):
        file_key = os.path.join(remain, base_dir, file_path.replace('%s/' % dirpath, ''))
        bucket_upload_file(bucket, file_path, file_key)


def _parse_bucket_uri(bucket_uri):
    """
    @bucket_uri: bucket_name/...
    Parse a bucket_uri, return (bucket_name, remain)
    """
    slash = '/'
    comps = bucket_uri.rstrip(slash).split(slash, 1)
    bucket_name = comps[0]
    if len(comps) == 2:
        remain = comps[1]
    else:
        remain = ''
    return bucket_name, remain


def upload(src, bucket_uri, n=1, **kwargs):
    """
    copy a dir contents or a file to a s3 bucket.
    """
    if src.endswith('/*'):
        src = src.rstrip('/*')
        kwargs['pattern'] = r'.'

    src = os.path.abspath(src)
    if not (os.path.isfile(src) or os.path.isdir(src)):
        raise Exception('%s is not a directory or file.' % src)

    if os.path.isdir(src):
        # dir
        _upload_dir(src, bucket_uri, n=n, **kwargs)
    else:
        # file
        bucket_name, remain = _parse_bucket_uri(bucket_uri)
        if not is_bucket(bucket_name):
            raise BucketNotExist(bucket_name)
        location = kwargs.get('location', 'cn-north-1')
        bucket = bucket_cache.setdefault(bucket_name,
                                         make_bucket(bucket_name, location=location))
        # file key
        if '.' in os.path.basename(remain):
            bucket.upload_file(src, remain)
        else:
            filename = os.path.basename(src)
            bucket.upload_file(src, os.path.join(remain, filename))


def _bucket_download_file(bucket, key, file_path):
    bucket.download_file(key, file_path)


def download(bucket_uri, dest, n=1, **kwargs):
    """
    copy s3 objects to dirpath.
    """
    if bucket_uri.endswith('/*'):
        bucket_uri = bucket_uri.rstrip('/*')
        kwargs['pattern'] = r''

    bucket_name, remain = _parse_bucket_uri(bucket_uri)
    if not is_bucket(bucket_name):
        raise BucketNotExist(bucket_name)

    dest_path = os.path.abspath(dest)
    dest_is_dir = False
    if os.path.isdir(dest):
        dest_is_dir = True

    location = kwargs.get('location', 'cn-north-1')
    bucket = bucket_cache.setdefault(bucket_name,
                                     make_bucket(bucket_name, location))

    n = 1 if is_key(bucket_name, remain) else n
    if n > 1:
        bucket_download_file = threads(n=n)(_bucket_download_file)
    else:
        bucket_download_file = _bucket_download_file

    if not dest_is_dir:
        if is_key(bucket_name, remain):
            file_path = dest_path
            bucket_download_file(bucket, remain, file_path)
        else:
            raise KeyNotExist(remain)
    else:
        # dest is dir
        if is_key(bucket_name, remain):
            file_path = os.path.join(dest_path, os.path.basename(remain))
            bucket_download_file(bucket, remain, file_path)
        else:
            # bucket or bucket pattern
            pattern = kwargs.get('pattern')
            if remain:
                bucket_objs = bucket.objects.filter(Prefix=remain)
                old_p = remain
            else:
                # remain is ''
                bucket_objs = bucket.objects.all()
                old_p = bucket_name

            if pattern is None:
                base_dir = os.path.basename(old_p)
            else:
                base_dir = ''
            # base_dir = os.path.basename(old_p) if pattern is None else ''
            for obj in bucket_objs:
                # scan obj.key
                if pattern and not re.search(pattern, obj.key):
                    continue
                file_path = os.path.join(dest_path, base_dir, obj.key.replace('%s/' % old_p, ''))
                mkdirs(os.path.dirname(file_path))
                bucket_download_file(bucket, obj.key, file_path)


def count(bucket_uri, **kwargs):
    """
    count objects number in bucket.
    """
    bucket_name, remain = _parse_bucket_uri(bucket_uri)
    if not is_bucket(bucket_name):
        raise BucketNotExist(bucket_name)

    location = kwargs.get('location', 'cn-north-1')
    bucket = bucket_cache.setdefault(bucket_name,
                                     make_bucket(bucket_name, location))

    # remain is ''
    if not remain:
        bucket_objs = bucket.objects.all()
    else:
        bucket_objs = bucket.objects.filter(Prefix=remain)

    pattern = kwargs.get('pattern')
    if pattern is None:
        number = sum(1 for _ in bucket_objs)
    else:
        number = sum(1 for obj in bucket_objs if re.search(pattern, obj.key))
    return number


class BucketNotExist(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Bucket "%s" not exist.' % self.name


class KeyNotExist(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Key "%s" not exist.' % self.name


if __name__ == '__main__':
    import time

    start = time.time()
    # download('dxwind-test/pt/', '/home/yh', n=20)
    end = time.time()
    print 'Time: %s seconds' % (end - start)
