#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
import mimetypes
import os.path

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.header import Header


class SMTPMail(object):
    def __init__(self, sender, passwd,
                 server="smtp.exmail.qq.com", port=25):
        try:
            smtp = smtplib.SMTP(server, port)
            smtp.login(sender, passwd)
            self._sender = sender
            self.smtp = smtp
        except Exception as e:
            raise e

    def send_mail(self, receivers, cc_list=None,
                  subject=None, content=None, attachs=None):
        # 构造邮件
        email = MIMEMultipart()
        email["from"] = self._sender
        cc_list = [] if cc_list is None else cc_list
        for to, cc in map(None, receivers, cc_list):
            if to is not None:
                email["to"] = to
            if cc is not None:
                email["cc"] = cc
        email["subject"] = Header(subject, "utf-8")
        encode_content = MIMEText(content, _charset="utf-8")
        email.attach(encode_content)

        # 附件
        attachs = [] if attachs is None else attachs
        for filepath in attachs:
            if not os.path.isfile(filepath):
                continue
            ctype, encoding = mimetypes.guess_type(filepath)
            if ctype is None or encoding is not None:
                ctype = "application/ostet-stream"
            maintype, subtype = ctype.split("/", 1)
            if maintype == "text":
                with open(filepath, "rb") as fp:
                    msg = MIMEText(fp.read(), subtype)
            elif maintype == "image":
                with open(filepath, "rb") as fp:
                    msg = MIMEImage(fp.read(), subtype)
            elif maintype == "audio":
                with open(filepath, "rb") as fp:
                    msg = MIMEAudio(fp.read(), subtype)
            else:
                with open(filepath, "rb") as fp:
                    msg = MIMEBase(maintype, subtype)
                    msg.set_payload(fp.read())
                    encoders.encode_base64(msg)
            filename = os.path.basename(filepath)
            msg.add_header("Content-Disposition", "attachment", filename=filename)
            email.attach(msg)

        # 发送邮件
        try:
            receivers.extend(cc_list)
            self.smtp.sendmail(self._sender, receivers, email.as_string())
        except Exception as e:
            raise e

    def close(self):
        self.smtp.quit()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


if __name__ == "__main__":
    # sender = "windresource@dxwind.com"
    # receivers = ["yanheng880918@163.com"]
    # cclist = ["yh@dxwind.com"]
    # passwd = "wras123456"
    # subject = "测试邮件"
    # content = "本邮件每天自动发送, 如有问题, 联系yh@dxwind.com"
    # with SMTPMail(sender, passwd) as smtp:
    #     smtp.send_mail(receivers, cc_list=cclist, subject=subject, content=content)
    pass
