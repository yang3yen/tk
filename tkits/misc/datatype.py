#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np

from collections import deque
from array import array as _array


__all__ = ["Singleton", "NullClass", "Null", "isnull",
           "isfloat", "isinteger", "isstring", "isseq"]


class Singleton(object):
    """singleton class."""
    def __new__(cls, *args, **kwargs):
        if "_inst" not in vars(cls):
            cls._inst = super(Singleton, cls).__new__(cls, *args)
        return cls._inst


class NullClass(Singleton):
    """null class."""
    def __init__(self, *args, **kwargs):
        pass

    # object calling
    def __call__(self, *args, **kwargs):
        return self

    # misc
    def __str__(self):
        return "Null"

    def __repr__(self):
        return "Null"

    # attribute handling
    def __getattr__(self, name):
        return self

    def __setattr__(self, name, value):
        return self

    def __delattr__(self, name):
        return self

    def __nonzero__(self):
        return False

    def __len__(self):
        return 0

    def __iter__(self):
        return iter(())

    # item handling
    def __getitem__(self, name):
        return self

    def __setitem__(self, name, value):
        return self

    def __delitem__(self, name):
        return self


Null = NullClass()


def isnull(value):
    return isinstance(value, (type(None), NullClass))


def isinteger(value):
    return isinstance(value, (int, long, np.integer))


def isfloat(value):
    return isinstance(value, (float, np.float))


def isstring(value):
    return isinstance(value, (str, unicode))


def isseq(value):
    return isinstance(value, (tuple, list, bytearray, buffer,
                              xrange, deque, _array,
                              np.ndarray, np.recarray))
