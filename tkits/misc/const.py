#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys


# 当const模块被其他模块引入时， __name__此时是模块名'const'
_name = __name__


class _const(object):
    class ConstError(TypeError):
        pass

    def __init__(self, module_name):
        self._name = module_name

    def __getattr__(self, name):
        if name not in self.__dict__:
            raise AttributeError('%s has no attribue %s' % (self._name, name))

    def __setattr__(self, name, value):
        # 禁止属性修改，允许新增属性
        if name in self.__dict__:
            raise self.ConstError("Can't rebind const(%s)" % name)
        self.__dict__[name] = value

    def __delattr__(self, name):
        # 禁止属性删除
        if name in self.__dict__:
            raise self.ConstError("Can't unbind const(%s)" % name)
        raise AttributeError('%s has no attribue %s' % (self._name, name))


# 原模块对象（const）被替换为类_const的实例
sys.modules[_name] = _const(_name)
