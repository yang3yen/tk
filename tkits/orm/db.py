#!/usr/bin/env python
# -*- coding: utf-8 -*-


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dialect import BaseDialect, PGDialect, SQLiteDialect


__all__ = ['DBManger']


class DBManger(object):

    def __init__(self, db_name, opts=None):
        self.db_name = db_name
        if opts is None or not isinstance(opts, dict):
            raise TypeError('opts expect a dict')
        self.opts = opts

    @property
    def dialect(self):
        if self.db_name == 'postgresql':
            db = PGDialect(opts=self.opts)
        elif self.db_name == 'sqlite':
            db = SQLiteDialect(opts=self.opts)
        else:
            db = BaseDialect(opts=self.opts)
        return db.dialect

    @property
    def engine(self):
        return create_engine(self.dialect)

    @property
    def session(self):
        return sessionmaker(bind=self.engine)()
