#!/usr/bin/env python
# -*- coding: utf-8 -*-


__all__ = ['PGDialect', 'SQLiteDialect', 'BaseDialect']


class BaseDialect(object):
    _dialect_pattern = ''

    def __init__(self, opts=None):
        if opts is None or not isinstance(opts, dict):
            raise TypeError('opts expect a dict')
        self.opts = opts.copy()

    @property
    def dialect(self):
        return ''


class PGDialect(BaseDialect):
    _dialect_pattern = 'postgresql+%(driver)s://%(user)s:%(password)s' \
                       '@%(host)s:%(port)s/%(database)s%(options)s'

    def __init__(self, opts=None):
        super(PGDialect, self).__init__(opts=opts)
        self.driver = self.opts.pop('driver', 'psycopg2')
        self.user = self.opts.pop('user', '')
        self.password = self.opts.pop('password', '')
        self.host = self.opts.pop('host', '')
        self.port = self.opts.pop('port', 5432)
        self.database = self.opts.pop('database', '')

    @property
    def options(self):
        kv_pairs = ['%s=%s' % (k, v) for k, v in self.opts.iteritems()]
        if kv_pairs:
            options = '?' + ''.join(kv_pairs)
        else:
            options = ''
        return options

    @property
    def dialect(self):
        return self._dialect_pattern % dict(driver=self.driver,
                                            user=self.user,
                                            password=self.password,
                                            host=self.host,
                                            port=self.port,
                                            database=self.database,
                                            options=self.options)


class SQLiteDialect(BaseDialect):
    _dialect_pattern = 'sqlite+%(driver)s:///%(db_file)s'

    def __init__(self, opts=None):
        super(SQLiteDialect, self).__init__(opts=opts)
        self.driver = self.opts.pop('driver', 'pysqlite')
        self.db_file = self.opts.pop('db_file', '')

    @property
    def dialect(self):
        return self._dialect_pattern % dict(driver=self.driver,
                                            db_file=self.db_file)
