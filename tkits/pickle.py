#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os.path
import cPickle as _pickle


__all__ = ['dump', 'load']


def dump(obj, name, protocol=0):
    if isinstance(name, (file,)):
        _pickle.dump(obj, name, protocol=protocol)

    fp = open(name, mode='w')
    try:
        _pickle.dump(obj, fp, protocol=protocol)
    except Exception as e:
        raise e
    finally:
        fp.close()


def load(name):
    if isinstance(name, (file,)):
        return _pickle.load(name)
    if not os.path.isfile(name):
        raise IOError('%s is not a file.' % name)
    fp = open(name)
    try:
        obj = _pickle.load(fp)
    except Exception as e:
        raise e
    finally:
        fp.close()
    return obj
