#!/usr/bin/env python
# -*- coding: utf-8 -*-


import json as _json
import os.path
from Path import mkdirs


__all__ = ['load', 'dump']


def load(path, encoding='utf-8', *args, **kwargs):
    with open(path, 'r') as fp:
        obj = _json.load(fp, encoding=encoding, *args, **kwargs)
    return obj


def dump(obj, path, indent=4, *args, **kwargs):
    dirname = os.path.abspath(os.path.dirname(path))
    if not os.path.isdir(dirname):
        mkdirs(dirname)
    with open(path, 'w') as fp:
        _json.dump(obj, fp, indent=indent, *args, **kwargs)
    return path
