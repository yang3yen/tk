#! /usr/bin/python env
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import absolute_import
import re
import tzlocal
import pytz

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse


__all__ = ['local_now', 'as_timezone', 'datetime_range',
           'parse_timedelta', 'parse_datetime']


def local_now():
    """
    返回本地现在的日期和时间
    """
    local_tz = tzlocal.get_localzone()
    return local_tz.localize(datetime.now())


def as_timezone(dt, tz_name):
    """
    将dt转成给定时区的日期和时间
    """
    if getattr(dt, 'tzinfo') is None:
        local_tz = tzlocal.get_localzone()
        local_dt = local_tz.localize(dt)
    else:
        local_dt = dt
    return local_dt.astimezone(pytz.timezone(tz_name))


def datetime_range(begin_dt, end_dt, step='1h'):
    """
    Return a list of datetimes form begin_dt to end_dt with timedelta.
    """
    if not isinstance(begin_dt, datetime):
        begin_dt = parse_datetime(begin_dt)

    if not isinstance(end_dt, datetime):
        end_dt = parse_datetime(end_dt)

    if not isinstance(step, (timedelta, relativedelta)):
        step = parse_timedelta(step)

    dt_lists = []
    while begin_dt < end_dt:
        dt_lists.append(begin_dt)
        begin_dt += step
    return dt_lists


def parse_timedelta(time_str):
    """
    解析time_str为timedelta
    >>> parse_timedelta('1m')
    datetime.timedelta(0, 60)
    """
    _time_format = {
        'Y': 'years',
        'M': 'months',
        'W': 'weeks',
        'd': 'days',
        'h': 'hours',
        'm': 'minutes',
        's': 'seconds',  # default
        'ms': 'milliseconds',  # 毫秒
    }
    if not isinstance(time_str, (int, long, float, str, unicode, timedelta)):
        return
    if isinstance(time_str, (timedelta, )):
        return time_str
    elif isinstance(time_str, (int, long, float)):
        return timedelta(seconds=time_str)
    else:
        pass
    match = re.match(r'^(?P<value>[-+]?[0-9]*\.?[0-9]*)(?P<symbol>(?:[YMWdhms]?|ms)$)', time_str)
    if not match:
        delta = None
    else:
        value = match.group('value')
        value = float(value) if '.' in value else int(value)
        symbol = match.group('symbol')
        # 默认返回秒
        key_value = {_time_format.get(symbol, 'seconds'): value}
        if symbol in ('Y', 'M'):
            delta = relativedelta(**key_value)
        else:
            delta = timedelta(**{_time_format.get(symbol, 'seconds'): value})
    return delta


def parse_datetime(dt_str, force_datetime=True):
    """
    Try parse datetime str or integer or list of str/integer into datetimes.

    :param dt_str: single value or seq of:
                    int/str: YYYYMMDD[HH[MM][SS]], delimiters are also allowed.
                    e.g.: YYYY-MM-DD HH:MM:SS
                    Julian Days, e.g.: YYYYJJJ[...] is also OK.
                    datetime/date
    :param force_datetime:
                if True, return datetime even if input is date.
                if False: return date if input is date.
    :return: datetime or list of datetimes.
    """
    non_digit = re.compile(r'\D*')
    jfmt_str = '%Y%j%H%M%S'
    gfmt_str = '%Y%m%d%H%M%S'
    single = False
    if isinstance(dt_str, (int, long, str, unicode, date, datetime)):
        list_of_dt_str = [dt_str]
        single = True
    else:
        list_of_dt_str = dt_str
    list_of_res = []
    for dt_str in list_of_dt_str:
        if isinstance(dt_str, (datetime,)):
            list_of_res.append(dt_str)
            continue
        if isinstance(dt_str, (date,)):
            if force_datetime:
                list_of_res.append(
                    datetime.fromordinal(dt_str.toordinal())
                )
            else:
                list_of_res.append(dt_str)
            continue
        if isinstance(dt_str, (int, long)):
            dt_str = str(dt_str)

        if len(dt_str) == 10:
            if re.match(r'^\d{10}$', dt_str):
                dt_str = ''.join([dt_str, '00'])
            elif re.match(r'^\d{4}.\d{2}.\d{2}$', dt_str):
                dt_str = ''.join([dt_str, '00:00:00'])
            else:
                pass

        try:
            res = parse(dt_str)
        except ValueError:
            try:
                digit_only_str = re.sub(non_digit, '', dt_str)
                length = len(digit_only_str)
                if length >= 7:
                    # YYYYJJJ[...]
                    if length % 2 == 1:
                        fmt_str = jfmt_str[:4+length-7]
                    else:
                        # YYYYMMDD[...]
                        fmt_str = gfmt_str[:6+length-8]
                    res = datetime.strptime(digit_only_str, fmt_str)
                else:
                    res = parse(digit_only_str)
            except ValueError:
                res = None
        list_of_res.append(res)
    if single:
        return list_of_res[0]
    else:
        return list_of_res


if __name__ == '__main__':
    print(parse_timedelta('1s'))
