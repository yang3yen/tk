#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Temperature(object):
    coefficients = {
        # 热力学温度 c = (k + 0.0) * 1.0  - 273.15
        'c': (0.0, 1.0, -273.15),
        # 华氏温度  f = (k - 273.15)  * 1.8 + 32
        'f': (-273.15, 1.8, 32.0),
        # 兰金温度 r = (k + 0.0) * 1.8 + 0.0
        'r': (0.0, 1.8, 0.0)
     }

    def __init__(self, **kwargs):
        try:
            name, value = kwargs.popitem()
        except KeyError:
            # 无参数，默认k=0
            name, value = 'k', 0
        # 参数过多或没有识别
        if kwargs or name not in 'kcfr':
            kwargs[name] = value
            raise TypeError('invalid arguments %r' % kwargs)
        setattr(self, name, float(value))

    def __getattr__(self, name):
        eq = self.coefficients.get(name)
        if eq is None:
            raise AttributeError(name)
        return (self.k + eq[0]) * eq[1] + eq[2]

    def __setattr__(self, name, value):
        if name in self.coefficients:
            eq = self.coefficients[name]
            self.k = (value - eq[2]) / eq[1] - eq[0]
        elif name == 'k':
            # object.__setattr__(self, name, value)
            super(Temperature, self).__setattr__(name, value)
        else:
            raise AttributeError(name)

    def __str__(self):
        return '%s K' % self.k

    def __repr__(self):
        return 'Temperature (k=%r)' % self.k


if __name__ == '__main__':
    t = Temperature()
    print t.c

