#!/usr/bin/env python
# -*- coding: utf-8 -*-


class FixedGrid(object):
    def __init__(self, min_lon, max_lon,
                 min_lat, max_lat,
                 lon_delta, lat_delta):
        """
        固定网格的基本信息.
        :param min_lon: 最小经度
        :param max_lon: 最大经度
        :param min_lat: 最小纬度
        :param max_lat: 最大纬度
        :param lon_delta: 网格经度间隔
        :param lat_delta: 网格纬度间隔
        """
        self.min_lon = min_lon
        self.max_lon = max_lon
        self.min_lat = min_lat
        self.max_lat = max_lat
        self.lon_delta = lon_delta
        self.lat_delta = lat_delta

    def lonlat_to_ij(self, lon, lat, lon_asc=True, lat_asc=True):
        """
        找出经纬度所在网格, 返回网格编号i,j
        """
        i = j = None
        if (self.min_lon <= lon <= self.max_lon) and (self.min_lat <= lat <= self.max_lat):
            i = int(round((lon - self.min_lon) / self.lon_delta))
            if not lon_asc:
                max_i = int(round(self.max_lon - self.min_lon) / self.lon_delta)
                i = max_i - i
            j = int(round((lat - self.min_lat) / self.lat_delta))
            if not lat_asc:
                max_j = int(round(self.max_lat - self.min_lat) / self.lon_delta)
                j = max_j - j
        return i, j
