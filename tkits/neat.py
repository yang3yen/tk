#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import division
import numpy as np


__all__ = ['get_neat_value', 'limit_slice']

def get_neat_value(value, rate, floor=True):
    # floor=True, 向下取值, 否则， 向上取值
    return np.floor(value / rate) / (1 / rate) if floor else \
        np.ceil(value / rate + 1) / (1 / rate)


def limit_slice(min_value, max_vaule, min_limit, max_limit, step=None):
    if min_value <= max_limit and max_vaule >= min_limit:
        start = min_value if min_value > min_limit else min_limit
        end = max_vaule if max_vaule < max_limit else max_limit
        limit = slice(start, end, step)
    else:
        limit = slice(0, 0)
    return limit
