#!/usr/bin/env python
# -*- coding: utf-8 -*-


from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, DateTime, Boolean
from sqlalchemy import Sequence


__all__ = ['Base', 'Server']


Base = declarative_base()


class Server(Base):
    """服务器表"""

    __tablename__ = 'servers'

    id = Column(name='id', type_=Integer, primary_key=True,
                default=Sequence('master_id_seq'))
    # ip地址
    ip = Column(name='ip', type_=Text, default='')
    is_master = Column(name='is_master', type_=Boolean, default=False)
    is_alive = Column(name='is_alive', type_=Boolean, default=True)
    # 更新时间
    update_time = Column(name='update_time', type_=DateTime, default=datetime.utcnow())

    def __repr__(self):
        return '<Server(ip="%s", is_master=%s, ' \
               'is_alive=%s)>' % (self.ip, self.is_master, self.is_alive)
