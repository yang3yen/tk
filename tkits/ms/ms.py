#!/usr/bin/env python
# -*- coding: utf-8 -*-


from models import Server, Base
# from tkits.misc.ip import local_ip
from tkits.orm import DBManger


class BaseMaster(object):
    def __init__(self, opts=None):
        self.opts = opts

    def ip(self):
        pass

    def switch(self, ip):
        pass


class DBMaster(BaseMaster):
    def __init__(self, db_name, opts=None):
        super(DBMaster, self).__init__(opts=opts)
        self.db_name = db_name
        self.db = DBManger(db_name=db_name, opts=opts)
        # master_ip table
        self.table = Server
        self.engine = self.db.engine
        self.session = self.db.session
        # 创建表
        Base.metadata.create_all(self.engine)

    @property
    def master(self):
        """current master."""
        _master = self.session.query(self.table). \
            filter_by(is_master=True).first()
        return _master

    @property
    def ip(self):
        _ip = ''
        if self.master is not None:
            _ip = self.master.ip
        return _ip

    # def is_master_server(self):
    #     return local_ip() == self.ip
    def switch(self, master_or_ip):
        """switch master."""
        if isinstance(master_or_ip, basestring):
            new_master = self.table(ip=master_or_ip)
        else:
            raise ValueError('master_or_ip value error.')

        cur_master = self.master
        if cur_master is None:
            self.session.add(new_master)
        else:
            cur_master.is_master = False
            self.session.add(cur_master)
            self.session.add(new_master)
