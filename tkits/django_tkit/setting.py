#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.conf import settings


__all__ = ['get_setting']

def get_setting(attr, default=None):
    return getattr(settings, attr, default)
